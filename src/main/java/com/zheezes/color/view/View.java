package com.zheezes.color.view;

import java.util.Observable;
import java.util.Observer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Shell;

import com.zheezes.color.control.Control;

public class View implements Observer {
	protected Shell shell;
	protected Display display;
	protected Canvas canvas;
	protected Control control;

	public void open() {
		display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
	}

	protected void createContents() {
		shell = new Shell(SWT.DIALOG_TRIM);
		shell.setSize(500, 375);
		shell.setText("color");

		final Scale red = new Scale(shell, SWT.BORDER | SWT.VERTICAL);
		red.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				control.setRed(255 - red.getSelection());
			}
		});
		red.setBounds(315, 1, 44, 344);
		red.setMaximum(255);
		red.setSelection(255);
		red.setToolTipText("RED");

		final Scale green = new Scale(shell, SWT.BORDER | SWT.VERTICAL);
		green.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				control.setGreen(255 - green.getSelection());
			}
		});
		green.setBounds(382, 1, 44, 344);
		green.setMaximum(255);
		green.setSelection(255);
		green.setToolTipText("GREEN");

		final Scale blue = new Scale(shell, SWT.BORDER | SWT.VERTICAL);
		blue.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				control.setBlue(255 - blue.getSelection());
			}
		});
		blue.setBounds(448, 1, 44, 344);
		blue.setMaximum(255);
		blue.setSelection(255);
		blue.setToolTipText("BLUE");

		canvas = new Canvas(shell, SWT.NONE);
		canvas.setBackground(new Color(display, 0, 0, 0));
		canvas.setBounds(0, 0, 309, 344);
	}

	public void regeisterControl(Control control) {
		this.control = control;
	}

	public void update(Observable o, Object arg) {
		canvas.setBackground(new Color(display, (RGB) arg));
	}
}
