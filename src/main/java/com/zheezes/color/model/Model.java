package com.zheezes.color.model;

import java.util.Observable;

import org.eclipse.swt.graphics.RGB;

public class Model extends Observable {
	private RGB color;

	public Model() {
		color = new RGB(0, 0, 0);
	}

	public void setRed(int red) {
		color.red = red;
		setChanged();
	}

	public void setGreen(int green) {
		color.green = green;
		setChanged();
	}

	public void setBlue(int blue) {
		color.blue = blue;
		setChanged();
	}

	public void update() {
		this.notifyObservers(color);
	}
}
