package com.zheezes.color.control;

import com.zheezes.color.model.Model;
import com.zheezes.color.view.View;

public class Control {
	private Model model;
	private View view;

	public Control() {
		model = new Model();
		view = new View();
		model.addObserver(view);
		view.regeisterControl(this);
	}

	public void open() {
		view.open();
	}
	
	public void setRed(int red) {
		model.setRed(red);
		model.update();
	}

	public void setGreen(int green) {
		model.setGreen(green);
		model.update();
	}

	public void setBlue(int blue) {
		model.setBlue(blue);
		model.update();
	}

	/**
	 * java -XstartOnFirstThread -jar color.jar
	 */
	public static void main(String[] args) {
		Control instance = new Control();
		instance.open();
	}
}
